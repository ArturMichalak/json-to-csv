# JSON to CSV converter

[![pipeline status](https://gitlab.com/ArturMichalak/json-to-csv/badges/master/pipeline.svg)](https://gitlab.com/ArturMichalak/json-to-csv/-/commits/master)
[![coverage report](https://gitlab.com/ArturMichalak/json-to-csv/badges/master/coverage.svg)](https://gitlab.com/ArturMichalak/json-to-csv/-/commits/master)

test:

- py.test app --doctest-modules --pylint app -v --cov app --cov-report xml:coverage.xml

start in root catalog with prepared venv:

- insert json files in json/ catalog
- python app/conv.py
