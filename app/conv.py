﻿"""Convert json files to csv"""
import json
import csv
import glob
import os

def conv(in_catalog, out_catalog):
    """Convert json files from inCatalog to csv files in output catalog"""
    if not os.path.exists(in_catalog):
        os.makedirs(in_catalog)

    if not os.path.exists(out_catalog):
        os.makedirs(out_catalog)

    for filename in glob.glob(f"{in_catalog}/*.json"):
        print(f"Plik {filename} jest kompilowany\n")
        with open(filename, encoding="utf-8") as json_file:
            data = json.load(json_file)

        write_file = open(
            filename.replace(in_catalog, out_catalog).replace(".json", ".csv"),
            mode="w",
            encoding="utf-8",
            newline="",
        )

        writer = csv.writer(write_file, quotechar="'")

        count = 0

        for row in data:
            if count == 0:
                header = row.keys()
                writer.writerow(header)
                count += 1
            row_items = []
            for item in row.values():
                row_items.append(item if item else '""' if isinstance(item, str) else "")

            writer.writerow(row_items)

        print(f"Plik {filename} został skopiowany\n")
        write_file.close()


conv("json", "csv")
