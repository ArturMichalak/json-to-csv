"""App tests"""
from .conv import conv

def test_conv():
    """Check if test file is equal to predefined string"""
    conv('json-test', 'csv-test')
    opened_file = open("csv-test/test.csv", "r")
    assert opened_file.read() == '''file,id
xxx/xxx.png,100
yyy/yyy.png,200
zzz/zzz.png,300
'''
